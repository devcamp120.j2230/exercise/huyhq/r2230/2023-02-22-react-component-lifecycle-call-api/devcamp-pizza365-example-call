import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Fetch from './component/Fetch';
import Axios from './component/Axios';

function App() {
  return (
    <div className="container">
      {/* <Fetch/> */}
      <Axios/>
    </div>
  );
}

export default App;
