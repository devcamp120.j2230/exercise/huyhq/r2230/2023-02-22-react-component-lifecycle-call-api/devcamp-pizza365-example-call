import { Component } from "react";

class Fetch extends Component {
    fetchApi = async (url, body) => {
        const response = await fetch(url, body);
        const data = await response.json();
        return data;
    };

    getAllHandle = () => {
        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/orders")
            .then(result => console.log(result))
            .catch(error => console.log('error', error.message));
    };

    getByIdHandle = () => {
        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/orders/WYsj1dbenW")
        .then(result => console.log(result))
        .catch(error => console.log('error', error.message));
    };

    postByIdHandle = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        
        var raw = JSON.stringify({
          "kichCo": "small",
          "duongKinh": "20cm",
          "suon": 2,
          "salad": "200g",
          "loaiPizza": "seafood",
          "idVourcher": "12354",
          "thanhTien": 150000,
          "giamGia": 15000,
          "idLoaiNuocUong": "TRASUA",
          "soLuongNuoc": 2,
          "hoTen": "Linh",
          "email": "linh@yahoo",
          "soDienThoai": "0987654321",
          "diaChi": "Minh Khai",
          "loiNhan": "Quẹt thẻ",
          "trangThai": "open"
        });
        
        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };

        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions)
        .then(result => console.log(result))
        .catch(error => console.log('error', error.message));
    };

    updateByIdHandle = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        
        var raw = JSON.stringify({
          "trangThai": "confirmed"
        });
        
        var requestOptions = {
          method: 'PUT',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };

        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/orders/127441", requestOptions)
        .then(result => console.log(result))
        .catch(error => console.log('error', error.message));
    };

    getVoucherByIdHandle = () => {
        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/24864")
        .then(result => console.log(result))
        .catch(error => console.log('error', error.message));
    };

    getAllDrinkdHandle = () => {
        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/drinks")
        .then(result => console.log(result))
        .catch(error => console.log('error', error.message));
    };

    render() {
        return (
            <>
                <p id="cmt2dev">Test Page for Javascrip Tasks. F5 to run code. </p>
                <div className="row form-group">
                    <button className="btn btn-info m-1 col" onClick={this.getAllHandle}>Call api get all orders!</button>
                    <button className="btn btn-success m-1 col" onClick={this.postByIdHandle}>Call api create order!</button>
                    <button className="btn btn-warning m-1 col" onClick={this.getByIdHandle}>Call api get order by id!</button>
                    <button className="btn btn-primary m-1 col" onClick={this.updateByIdHandle}>Call api update order!</button>
                    <button className="btn btn-info m-1 col" onClick={this.getVoucherByIdHandle}>Call api check voucher by id!</button>
                    <button className="btn btn-danger m-1 col" onClick={this.getAllDrinkdHandle}>Call api Get drink list!</button>
                </div>
                <div className="row form-group">
                    <p id="testP" className="h4"> Demo 06 API for Pizza 365 Project: </p>
                </div>
                <div className="row form-group">
                    <ul>
                        <li>get all Orders: lấy tất cả orders </li>
                        <li>create Order: tạo 1 order</li>
                        <li>get Order by id: lấy 1 order bằng id </li>
                        <li>update Order: update 01 order</li>
                        <li>check voucher by id: check thông tin mã giảm giá, quan trọng là có hay không, và % giảm giá </li>
                        <li>get drink list: lấy danh sách đồ uống</li>
                    </ul>
                </div>
                <div className="row form-group">
                    <strong className="text-danger"> Bật console log để nhìn rõ output </strong>
                </div>
            </>
        );
    }
}

export default Fetch;